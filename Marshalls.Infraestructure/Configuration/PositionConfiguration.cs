﻿using Marshalls.Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Marshalls.Infraestructure.Configuration
{
    public class PositionConfiguration : IEntityTypeConfiguration<Position>
    {
        public void Configure(EntityTypeBuilder<Position> entity)
        {
            entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .IsUnicode(false);
        }
    }
}
