﻿using Marshalls.Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Marshalls.Infraestructure.Configuration
{
    public class OfficeConfiguration : IEntityTypeConfiguration<Office>
    {
        public void Configure(EntityTypeBuilder<Office> entity)
        {
            entity.Property(e => e.Letter)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("letter");

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);
        }
    }
}
