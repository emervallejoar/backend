﻿using Marshalls.Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Marshalls.Infraestructure.Configuration
{
    public class DivisionConfiguration : IEntityTypeConfiguration<Division>
    {
        public void Configure(EntityTypeBuilder<Division> entity)
        {
            entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .IsUnicode(false);
        }
    }
}
