﻿using Marshalls.Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Marshalls.Infraestructure.Configuration
{
    public class SalaryConfiguration : IEntityTypeConfiguration<Salary>
    {
        public void Configure(EntityTypeBuilder<Salary> entity)
        {
            entity.Property(e => e.BeginDate).HasColumnType("date");

            entity.Property(e => e.Birthday).HasColumnType("date");

            entity.Property(e => e.EmployeeCode)
                .HasMaxLength(10)
                .IsUnicode(false);

            entity.Property(e => e.EmployeeName)
                .HasMaxLength(150)
                .IsUnicode(false);

            entity.Property(e => e.EmployeeSurname)
                .HasMaxLength(150)
                .IsUnicode(false);

            entity.Property(e => e.IdentificationNumber)
                .HasMaxLength(10)
                .IsUnicode(false);

            entity.HasOne(d => d.DivisionNavigation)
                .WithMany(p => p.Salaries)
                .HasForeignKey(d => d.Division)
                .HasConstraintName("FK_Salaries_Divisions");

            entity.HasOne(d => d.OfficeNavigation)
                .WithMany(p => p.Salaries)
                .HasForeignKey(d => d.Office)
                .HasConstraintName("FK_Salaries_Offices");

            entity.HasOne(d => d.PositionNavigation)
                .WithMany(p => p.Salaries)
                .HasForeignKey(d => d.Position)
                .HasConstraintName("FK_Salaries_Positions");
        }
    }
}
