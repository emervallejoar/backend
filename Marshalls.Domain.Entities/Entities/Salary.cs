﻿using System;

#nullable disable

namespace Marshalls.Domain.Entities.Entities
{
    public partial class Salary
    {
        public int Id { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Office { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public int? Division { get; set; }
        public int? Position { get; set; }
        public int? Grade { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public double? BaseSalary { get; set; }
        public double? ProductionBonus { get; set; }
        public double? CompensationBonus { get; set; }
        public double? Commission { get; set; }
        public double? Contributions { get; set; }

        public virtual Division DivisionNavigation { get; set; }
        public virtual Office OfficeNavigation { get; set; }
        public virtual Position PositionNavigation { get; set; }
    }
}
