﻿using System.Collections.Generic;

#nullable disable

namespace Marshalls.Domain.Entities.Entities
{
    public partial class Division
    {
        public Division()
        {
            Salaries = new HashSet<Salary>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Salary> Salaries { get; set; }
    }
}
